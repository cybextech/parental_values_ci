var dbRefDevice = new Firebase(fb_url);
var dbRefDevicesReturn = dbRefDevice.child(fb_user_id);
var selected_or_not = "";

////////////////////////////////////////////////////////////////////////
//device fetcher
////////////////////////////////////////////////////////////////////////
dbRefDevicesReturn.on("child_added", function(snapshot) {
	
	var value = snapshot.val();
	var dev_id = snapshot.key();
	var device_div = "";
	selected_or_not = "";
	
	for (var key in value) {
		if(key == "connectivity"){
			if(dev_id == fb_dev_id){
				selected_or_not = "selected-device";
			}
			var battery = value[key]['battery_status'];
			var dev_name = value[key]['device_name'];
			var conn_status = value[key]['connection_status'];
			var generated_dev_div = device_div_create(dev_id, selected_or_not, dev_name, battery, conn_status);
			
			device_div = device_div + "<a class='unselected-device' href='"+site_url+"/home/index/set_dev_id/"+dev_id+"/"+dev_name+"' id='"+dev_id+"'>"+generated_dev_div+"</a>";
			$("#device_fetcher_spinner").hide();
		}
	}
	$("#devices").append(device_div);
});

dbRefDevicesReturn.on("child_changed", function(snapshot) {
	var value = snapshot.val();
	var dev_id = snapshot.key();
	var device_div = "";
	
	$("#"+dev_id).empty();
	selected_or_not = "";
	
	for (var key in value) {
		if(key == "connectivity"){
			if(dev_id == fb_dev_id){
				selected_or_not = "selected-device";
			}
			var battery = value[key]['battery_status'];
			var dev_name = value[key]['device_name'];
			var conn_status = value[key]['connection_status'];
			
			device_div = device_div_create(dev_id, selected_or_not, dev_name, battery, conn_status);
		}
	}
	$("#"+dev_id).append(device_div);
});

dbRefDevicesReturn.on("child_removed", function(snapshot) {
	var value = snapshot.val();
	var dev_id = snapshot.key();
	$("#"+dev_id).remove();				
	selected_or_not = "";
});
/////////////////////////////////////////////////////////////////////////

//method to generate device div
function device_div_create(dev_id, selected_or_not, dev_name, battery, conn_status){
	
	device_div = "<div id='"+dev_id+"_div' class='row device-show-div list-group-item "+selected_or_not+"'><div class='col-md-2'><span class='fa fa-mobile dev-list-dev-icon'></span></div><div class='col-sm-10'>"+dev_name+"<span class='fa fa-battery-3 pull-right dev-list-bat-icon'> "+battery+"</span><br />"+conn_status+"</div></div>";
	
	return device_div;
}