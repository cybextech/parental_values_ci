////////////////////////////////////////////////////////////////////////
//firebase stuff
////////////////////////////////////////////////////////////////////////
//create firebase reference for applications
var string = fb_url+"/"+fb_user_id+"/"+fb_dev_id;
var dbRef = new Firebase(string);
var dbRefApplicationsReturn = dbRef.child('applications');
var dbRefContactsReturn = dbRef.child('call_register/contacts');
var dbRefInternetReturn = dbRef.child('internet');
////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
//applications counter start
////////////////////////////////////////////////////////////////////////
var blocked_apps_counter = 0;
var total_apps_counter = 0;

dbRefApplicationsReturn.on("child_changed", function(snapshot) {
	var value = snapshot.val();
	for (var key in value){
		if(key == "app_is_blocked"){
			if(value[key] == true){
				blocked_apps_counter ++;
				$('#blocked_applications').empty();
				$('#blocked_applications').append(blocked_apps_counter);
			}
			else{
				blocked_apps_counter --;
				$('#blocked_applications').empty();
				$('#blocked_applications').append(blocked_apps_counter);
			}
		}
	}
});
				
dbRefApplicationsReturn.on("child_added", function(snapshot) {
	var value = snapshot.val();
	for (var key in value) {
		if(key == "app_is_blocked"){
			if(value[key] == true){
				blocked_apps_counter ++;
				$('#blocked_applications').empty();
				$('#blocked_applications').append(blocked_apps_counter);
			}
		}
	}
	total_apps_counter++;
	$('#total_applications').empty();
	$('#total_applications').append(total_apps_counter);
});

dbRefApplicationsReturn.on('child_removed', function(snapshot) {
	var value = snapshot.val();
	for (var key in value) {
		if(key == "app_is_blocked"){
			if(value[key] == true){
				blocked_apps_counter --;
				$('#blocked_applications').empty();
				$('#blocked_applications').append(blocked_apps_counter);
			}
		}
	}
	total_apps_counter--;
	$('#total_applications').empty();
	$('#total_applications').append(total_apps_counter);
});
////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
//contacts counter start
////////////////////////////////////////////////////////////////////////

var blocked_contacts_counter = 0;
var total_contacts_counter = 0;

dbRefContactsReturn.on("child_changed", function(snapshot) {
	var value = snapshot.val();
	for (var key in value){
		if(key == "is_blocked"){
			if(value[key] == true){
				blocked_contacts_counter ++;
				$('#blocked_contacts').empty();
				$('#blocked_contacts').append(blocked_contacts_counter);
			}
			else{
				blocked_contacts_counter --;
				$('#blocked_contacts').empty();
				$('#blocked_contacts').append(blocked_contacts_counter);
			}
		}
	}
});
				
dbRefContactsReturn.on("child_added", function(snapshot) {
	var value = snapshot.val();
	for (var key in value) {
		if(key == "is_blocked"){
			if(value[key] == true){
				blocked_contacts_counter ++;
				$('#blocked_contacts').empty();
				$('#blocked_contacts').append(blocked_contacts_counter);
			}
		}
	}
	total_contacts_counter++;
	$('#total_contacts').empty();
	$('#total_contacts').append(total_contacts_counter);
});

dbRefContactsReturn.on('child_removed', function(snapshot) {
	var value = snapshot.val();
	for (var key in value) {
		if(key == "is_blocked"){
			if(value[key] == true){
				blocked_contacts_counter --;
				$('#blocked_contacts').empty();
				$('#blocked_contacts').append(blocked_contacts_counter);
			}
		}
	}
	total_contacts_counter--;
	$('#total_contacts').empty();
	$('#total_contacts').append(total_contacts_counter);
});
////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
//internet counter start
////////////////////////////////////////////////////////////////////////

var blocked_sites_counter = 0;
var total_sites_counter = 0;

dbRefInternetReturn.on("child_changed", function(snapshot) {
	var value = snapshot.val();
	for (var key in value){
		if(key == "is_blocked"){
			if(value[key] == true){
				blocked_sites_counter ++;
				$('#blocked_websites').empty();
				$('#blocked_websites').append(blocked_sites_counter);
			}
			else{
				blocked_sites_counter --;
				$('#blocked_websites').empty();
				$('#blocked_websites').append(blocked_sites_counter);
			}
		}
	}
});
				
dbRefInternetReturn.on("child_added", function(snapshot) {
	var value = snapshot.val();
	for (var key in value) {
		if(key == "is_blocked"){
			if(value[key] == true){
				blocked_sites_counter ++;
				$('#blocked_websites').empty();
				$('#blocked_websites').append(blocked_sites_counter);
			}
		}
	}
	total_sites_counter++;
	$('#total_websites').empty();
	$('#total_websites').append(total_sites_counter);
});

dbRefInternetReturn.on('child_removed', function(snapshot) {
	var value = snapshot.val();
	for (var key in value) {
		if(key == "is_blocked"){
			if(value[key] == true){
				blocked_sites_counter --;
				$('#blocked_websites').empty();
				$('#blocked_websites').append(blocked_sites_counter);
			}
		}
	}
	total_sites_counter--;
	$('#total_websites').empty();
	$('#total_websites').append(total_sites_counter);
});
////////////////////////////////////////////////////////////////////////