var user_id = "";
var device_id = "";

user_id = fb_user_id;
device_id = fb_dev_id;

$(".contact_dat").change(function() {
	//getting app id
	var contact_id = $(this).attr("data");
	//getting category type
	var cat_type = $(this).attr("cat_type");
	//generating loader gif id from contact_id
	var loader_contact_id = "#" + contact_id + "_load" + cat_type;
	//call to change status of contact, enable/disable
	$.ajax({
		url: site_url+ "/calls/contact_enable_disable",
		type: 'POST',
		data: {action:'contact_enable_disable', user_id:user_id, device_id:device_id ,contact_id:contact_id},
		beforeSend: function() {
			$("#"+contact_id+cat_type).attr("disabled", true);
			$(loader_contact_id).show();
		},
		success: function (state_change_response) {
			var response = $.parseJSON(state_change_response);
			
			$("#"+contact_id+cat_type).removeAttr("disabled");
			$(loader_contact_id).hide(200);
			if(response.contact_status == true){
				$("#"+contact_id).prop('checked', false);
				$("#"+contact_id+cat_type).prop('checked', false);
			}
			else{
				$("#"+contact_id).prop('checked', true);
				$("#"+contact_id+cat_type).prop('checked', true);
			}
		}
	}); 
});