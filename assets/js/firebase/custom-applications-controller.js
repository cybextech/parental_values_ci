var user_id = "";
var device_id = "";

user_id = fb_user_id;
device_id = fb_dev_id;

$(".application_dat").change(function() {
	//getting app id
	var app_id = $(this).attr("data");
	//getting category type
	var cat_type = $(this).attr("cat_type");
	//generating loader gif id from app_id
	var loader_app_id = "#" + app_id + "_load" + cat_type;
	//call to change status of app, enable/disable
	$.ajax({
		url: site_url+ "/applications/application_enable_disable",
		type: 'POST',
		data: {action:'app_enable_disable', user_id:user_id, device_id:device_id , app_id:app_id},
		beforeSend: function() {
			$("#"+app_id+cat_type).attr("disabled", true);
			$(loader_app_id).show();
		},
		success: function (state_change_response) {
			var response = $.parseJSON(state_change_response);
			$("#"+app_id+cat_type).removeAttr("disabled");
			$(loader_app_id).hide(200);
			if(response.app_status == true){
				$("#"+app_id).prop('checked', false);
				$("#"+app_id+cat_type).prop('checked', false);
			}
			else{
				$("#"+app_id).prop('checked', true);
				$("#"+app_id+cat_type).prop('checked', true);
			}
		}
	}); 
});