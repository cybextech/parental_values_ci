var user_id = fb_user_id;
var vid_caption;
var vid_description;
var img_caption;
var img_description;

$("#vid_upload").fileinput({ 
	fileActionSettings: {
		showZoom: false,
		showUpload:false,
		indicatorNew: ' '
	},
	uploadExtraData:function() { 
						vid_caption = $("#vid_caption").prop('value');
						vid_description = $("#vid_description").prop('value');
						var out = {};
						out = {user_id : user_id, media_caption : vid_caption, media_description : vid_description, media_type:'video'};
						return out;
	} 
}); 

$("#img_upload").fileinput({ 
	fileActionSettings: {
			showZoom: false,
			showUpload:false,
			indicatorNew: ' '
	},
	uploadExtraData:function() { 
						vid_caption = $("#img_caption").prop('value');
						vid_description = $("#img_description").prop('value');
						var out = {};
						out = {user_id : user_id, media_caption : vid_caption, media_description : vid_description, media_type:'image'};
						return out;
	} 
});

//saves text
function save_text(){
	text_subject = $("#text_subject").prop('value');
	text_content = CKEDITOR.instances.multimedia_text.getData();
	
	alert(text_subject+" "+text_content);
	
	$.ajax({
			url: site_url+ "/multimedia/save_text",
			type: 'POST',
			data: {action : 'text', user_id : user_id , subject : text_subject , content : text_content},
			beforeSend: function() {
			// put code here for before send operation as loaders
				
			},
			success: function (registration_response) {
				//alert('before send');
				//$("#loading").hide(200);
				console.log(registration_response);
				var response = $.parseJSON(registration_response);
				alert(response.message);
				// $("#registration_response_area").append(response.message);
				// $("#registration_response_area").show(300);
			}
		}); 
}