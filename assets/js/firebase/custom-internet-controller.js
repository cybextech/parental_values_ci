var user_id = "";
var device_id = "";

user_id = fb_user_id;
device_id = fb_dev_id;

$(".internet_dat").change(function() {
	//getting app id
	var site_id = $(this).attr("data");
	//getting category type
	var cat_type = $(this).attr("cat_type");
	//generating loader gif id from site_id
	var loader_site_id = "#" + site_id + "_load" + cat_type;
	//call to change status of app, enable/disable
	$.ajax({
		url: site_url+ "/internet/internet_enable_disable",
		type: 'POST',
		data: {action:'internet_enable_disable', user_id:user_id, device_id:device_id ,site_id:site_id},
		beforeSend: function() {
			$("#"+site_id+cat_type).attr("disabled", true);
			$(loader_site_id).show();
		},
		success: function (state_change_response) {
			
			var response = $.parseJSON(state_change_response);
			
			$("#"+site_id+cat_type).removeAttr("disabled");
			$(loader_site_id).hide(200);
			if(response.site_status == true){
				$("#"+site_id).prop('checked', false);
				$("#"+site_id+cat_type).prop('checked', false);
			}
			else{
				$("#"+site_id).prop('checked', true);
				$("#"+site_id+cat_type).prop('checked', true);
			}
		}
	}); 
});