<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Applications Class
 * @author	cybextech
 */
class Applications extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
		
        //session_check();
        $this->load->model('firebase_model');
        $this->load->model('applications_model');
		$this->session->set_userdata('selected_tile', 'applications');
    }


    /** default function of the controller */
    public function index()
    {	
		if($this->session->userdata('user_id') !== FALSE && $this->session->userdata('device_id') == FALSE){
			$data['page_title'] = $this->lang->line('applications');
			$data['view'] = 'applications_no_device';
			$this->load->view('template', $data);
		}
		else{
			
			$applications_response = $this->firebase_model->bring_applications($this->session->userdata('user_id'), $this->session->userdata('device_id'));
			$applications_details_content = $this->applications_model->make_applications_tabs($applications_response);
			$data['page_title'] = $this->lang->line('applications');
			$data['view'] = 'applications';
			$data['applications_tabs'] = $applications_details_content;
			$data['device_name'] = $this->session->userdata('device_name');
			
			$this->load->view('template', $data);
		}
    }
	
	//ajax method for enabling and disabling website
	public function application_enable_disable(){
		
		$user_id = $this->input->post('user_id');
		$device_id = $this->input->post('device_id');
		$app_id = $this->input->post('app_id');
		
		$response = $this->firebase_model->update_app_status($user_id, $device_id, $app_id);
		$updated_app_status = json_decode($response, true);
		$updated_app_status = $updated_app_status['app_is_blocked'];
		
		$json['app_status'] = $updated_app_status;
		echo json_encode($json);
	}	
}

/* End of file Applications.php */
/* Location: ./application/controllers/Applications.php */