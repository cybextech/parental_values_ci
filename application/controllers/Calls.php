<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Calls Class
 * @author	cybextech
 */
class Calls extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
		
        //session_check();
        $this->load->model('firebase_model');
        $this->load->model('calls_model');
		$this->session->set_userdata('selected_tile', 'calls');
    }
	
    /** default function of the controller */
    public function index()
    {	
		if($this->session->userdata('user_id') !== FALSE && $this->session->userdata('device_id') == FALSE){
			$data['page_title'] = $this->lang->line('calls');
			$data['view'] = 'calls_no_device';
			$this->load->view('template', $data);
		}
		else{
			
			$contacts_response = $this->firebase_model->bring_contacts($this->session->userdata('user_id'), $this->session->userdata('device_id'));
			$calls_details_content = $this->calls_model->make_call_register_n_contacts_tabs($contacts_response);
			$data['page_title'] = $this->lang->line('calls');
			$data['view'] = 'calls';
			$data['calls_n_contacts_tabs'] = $calls_details_content;
			$data['device_name'] = $this->session->userdata('device_name');
			$this->load->view('template', $data);
		}
    }
	
	//ajax method for enabling and disabling website
	public function contact_enable_disable(){
		
		$user_id = $this->input->post('user_id');
		$device_id = $this->input->post('device_id');
		$contact_id = $this->input->post('contact_id');
		
		$response = $this->firebase_model->update_contact_status($user_id, $device_id, $contact_id);
		$updated_contact_status = json_decode($response, true);
		$updated_contact_status = $updated_contact_status['is_blocked'];
		
		$json['contact_status'] = $updated_contact_status;
		echo json_encode($json);
	}
}

/* End of file Calls.php */
/* Location: ./application/controllers/Calls.php */