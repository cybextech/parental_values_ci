<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Multimedia Class
 * @author	cybextech
 */
class Multimedia extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
		
        //session_check();
        $this->load->model('firebase_model');
        $this->load->model('multimedia_model');
		$this->session->set_userdata('selected_tile', 'multimedia');
    }
	
    /** default function of the controller */
    public function index()
    {	
		if($this->session->userdata('user_id') !== FALSE && $this->session->userdata('device_id') == FALSE){
			$data['page_title'] = $this->lang->line('calls');
			$data['view'] = 'calls_no_device';
			$this->load->view('template', $data);
		}
		else{
			// $contacts_response = $this->firebase_model->bring_contacts($this->session->userdata('user_id'), $this->session->userdata('device_id'));
			// $calls_details_content = $this->calls_model->make_call_register_n_contacts_tabs($contacts_response);
			$data['page_title'] = $this->lang->line('multimedia');
			$data['view'] = 'multimedia';
			//$data['calls_n_contacts_tabs'] = $calls_details_content;
			$data['device_name'] = $this->session->userdata('device_name');
			$this->load->view('template', $data);
		}
    }	
	
	public function handle_multimedia(){
		$resp = $this->multimedia_model->save_multimedia();
		if($resp){
			$resp = json_decode($resp);
			$fire_resp = $this->firebase_model->save_multimedia($resp->user_id, $resp->title, $resp->url, /* $resp->media_category, */ $resp->media_type, $resp->is_active, $resp->media_key, $resp->media_caption, $resp->media_description);
			if($fire_resp = true){
				$return['success'] ='uploaded';
				echo json_encode($return);
			}
			else{
				$return['error'] ='Oops! Something went wrong, please try again.';
				echo json_encode($return);
			}
		}
	}
	
	public function save_text(){
		$user_id = $this->input->post('user_id');
		$subject = $this->input->post('subject');
		$media_type = $this->input->post('action');
		$media = $this->input->post('content');
		$is_active = true;
		$media_key = md5(uniqid());
		
		$response = $this->firebase_model->save_text($user_id, $subject, $media_type, $is_active, $media_key, $media);
		if($response = true){
			$return['message'] ='saved';
			echo json_encode($return);
			die;
		}
		else{
			$return['message'] ='not saved';
			echo json_encode($return);
			die;
		}
	}
	
}

/* End of file Multimedia.php */
/* Location: ./application/controllers/Multimedia.php */