<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Home Class
 * @author	cybextech
 */
class Home extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
		//setting hard coded vals for testing, remove after completeion
		if(!$this->session->userdata('firebase_base_url', 'user_id', 'device_id')){
			$this->session->set_userdata('firebase_base_url', 'https://parental-values-test-db.firebaseio.com');
			$this->session->set_userdata('user_id', 'hKOWWDfZSUQVieaoeKvfuny1XNE3');
			$this->session->set_userdata('device_id', '357377050426585');
			$this->session->set_userdata('device_name', '"Galaxy A5"');
		}
        //session_check();
        $this->load->model('home_model');
		$this->session->set_userdata('selected_tile', 'home');
    }


    /** default function of the controller */
    public function index($type = '', $value1 = 0, $value2 = "")
    {
		//call method to set device id in session when type is set_dev_id
		if($type !== '' && $type === 'set_dev_id'){
			$this->home_model->set_device_id_session($value1, $value2);
		}
		
		//view dashboard page
		else{
			$data['page_title'] = $this->lang->line('home');
			$data['view'] = 'dashboard';
			$this->load->view('template', $data);
		}
        
    }
	
	public function test(){
		echo 'hola';
	}

}

/* End of file Home.php */
/* Location: ./application/controllers/Home.php */