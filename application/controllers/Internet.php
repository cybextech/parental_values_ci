<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Internet Class
 * @author	cybextech
 */
class Internet extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
		
        //session_check();
        $this->load->model('firebase_model');
        $this->load->model('internet_model');
		$this->session->set_userdata('selected_tile', 'internet');
    }


    /** default function of the controller */
    public function index()
    {	
		if($this->session->userdata('user_id') !== FALSE && $this->session->userdata('device_id') == FALSE){
			$data['page_title'] = $this->lang->line('internet');
			$data['view'] = 'internet_no_device';
			$this->load->view('template', $data);
		}
		else{
			$internet_response = $this->firebase_model->bring_internet($this->session->userdata('user_id'), $this->session->userdata('device_id'));
			$internet_details_content = $this->internet_model->make_internet_tabs($internet_response);
			$data['page_title'] = $this->lang->line('internet');
			$data['view'] = 'internet';
			$data['internet_tabs'] = $internet_details_content;
			$data['device_name'] = $this->session->userdata('device_name');
			
			$this->load->view('template', $data);
		}
		
    }
	
	//ajax method for enabling and disabling website
	public function internet_enable_disable(){
		
		$user_id = $this->input->post('user_id');
		$device_id = $this->input->post('device_id');
		$site_id = $this->input->post('site_id');
		
		$response = $this->firebase_model->update_site_status($user_id, $device_id, $site_id);
		$updated_site_status = json_decode($response, true);
		$updated_site_status = $updated_site_status['is_blocked'];
		
		$json['site_status'] = $updated_site_status;
		echo json_encode($json);
	}
}

/* End of file Internet.php */
/* Location: ./application/controllers/Internet.php */