<?php $this->load->view('header'); ?>

<?php 
	$selected_tile = $this->session->userdata('selected_tile');
?>
<!--sidebar start-->
<div class="row sidebar-container">
	<!--menu bar-->
	<div id="left_menu" class="col-md-2 col-sm-2 scroll left-menu">						
		<!-- home div start -->
		<a href="<?php echo site_url('home/index') ?>">
			<div class="col-md-12">
				<div class="panel panel-default home_main no-bottom-margin <?php echo ($selected_tile === 'home') ? '' : 'left-tile' ?>" data="home">
					<div class="panel-heading tile-heading">
						<h3 class="panel-title text-white"><i class="fa fa-home">&nbsp</i> Home</h3>
						<i class="fa fa-arrow-right pull-right tile-arrow">&nbsp </i>
					</div>
					<div class="panel-body home home_1 <?php echo ($selected_tile === 'home') ? 'tile-hidden' : 'tile-visible' ?>"></div>
					<div class="panel-body home_2 <?php echo ($selected_tile === 'home') ? 'sec-tile-visible' : 'tile-hidden' ?>">
						<span class="text-center">
							<br />
							<h4 class="text-white">Statistics</h4>
						</span>
					</div>
				</div>
			</div>
		</a>
		<!-- home div end -->
		<!-- internet div start -->
		<a href="<?php echo site_url('internet/index') ?>">
			<div class="col-md-12">
				<div class="panel panel-default internet_main no-bottom-margin <?php echo ($selected_tile === 'internet') ? '' : 'left-tile' ?>" data="internet">
					<div class="panel-heading tile-heading">
						<h3 class="panel-title text-white"><i class="fa fa-globe">&nbsp</i> Internet</h3>
						<i class="fa fa-arrow-right pull-right tile-arrow">&nbsp </i>
					</div>
					<div class="panel-body internet internet_1 <?php echo ($selected_tile === 'internet') ? 'tile-hidden' : 'tile-visible' ?>"></div>
					<div class="panel-body internet_2 <?php echo ($selected_tile === 'internet') ? 'sec-tile-visible' : 'tile-hidden' ?>">
						<span class="text-center">
							<br />
							<h4 class="text-white">Total Websites : <span id="total_websites" class="stat-number">0</span></h4>
							<hr class="tile-stat-hr">
							<h4 class="text-white">Blocked Websites : <span id="blocked_websites" class="stat-number">0</span></h4>
						</span>
					</div>
				</div>
			</div>
		</a>
		<!-- internet div end -->
		<!-- applications div start -->
		<a href="<?php echo site_url('applications/index') ?>">
			<div class="col-md-12">
				<div class="panel panel-default applications_main <?php echo ($selected_tile === 'applications') ? '' : 'left-tile' ?> no-bottom-margin" data="applications">
					<div class="panel-heading tile-heading">
						<h3 class="panel-title text-white"><i class="fa fa-th">&nbsp</i> Applications</h3>
						<i class="fa fa-arrow-right pull-right tile-arrow">&nbsp </i>
					</div>
					<div class="panel-body applications applications_1 <?php echo ($selected_tile === 'applications') ? 'tile-hidden' : 'tile-visible' ?>"></div>
					<div class="panel-body applications_2 <?php echo ($selected_tile === 'applications') ? 'sec-tile-visible' : 'tile-hidden' ?>">  
						<span class="text-center">
							<br />
							<h4 class="text-white">Total Apps : <span id="total_applications" class="stat-number">0</span></h4>
							<hr class="tile-stat-hr">
							<h4 class="text-white">Locked Apps : <span id="blocked_applications" class="stat-number">0</span></h4>
						</span>
					</div>
				</div>
			</div>
		</a>
		<!-- applications div end -->
		<!-- calls div start -->
		<a href="<?php echo site_url('calls/index') ?>">
			<div class="col-md-12">
				<div class="panel panel-default calls_main no-bottom-margin <?php echo ($selected_tile === 'calls') ? '' : 'left-tile' ?>" data="calls">
					<div class="panel-heading tile-heading">		
						<h3 class="panel-title text-white"><i class="fa fa-phone">&nbsp</i> Calls</h3>
						<i class="fa fa-arrow-right pull-right tile-arrow">&nbsp </i>
					</div>
					<div class="panel-body calls calls_1 <?php echo ($selected_tile === 'calls') ? 'tile-hidden' : 'tile-visible' ?>"></div>
					<div class="panel-body calls_2 <?php echo ($selected_tile === 'calls') ? 'sec-tile-visible' : 'tile-hidden' ?>">
						<span class="text-center">
							<br />
							<h4 class="text-white">Total Contacts : <span id="total_contacts" class="stat-number">0</span></h4>
							<hr class="tile-stat-hr">
							<h4 class="text-white">Locked Contacts : <span id="blocked_contacts" class="stat-number">0</span></h4>
						</span>
					</div>
				</div>
			</div>
		</a>
		<!-- calls div end -->
		<!-- alarms div start -->
		<a href="<?php echo site_url('alarms/index') ?>">
			<div class="col-md-12">
				<div class="panel panel-default alarms_main no-bottom-margin <?php echo ($selected_tile === 'alarms') ? '' : 'left-tile' ?>" data="alarms">
					<div class="panel-heading tile-heading">
						<h3 class="panel-title text-white"><i class="fa fa-bell">&nbsp</i> Alarms</h3>
						<i class="fa fa-arrow-right pull-right tile-arrow">&nbsp </i>
					</div>
					<div class="panel-body alarms alarms_1 <?php echo ($selected_tile === 'alarms') ? 'tile-hidden' : 'tile-visible' ?>"></div>
					<div class="panel-body alarms_2 <?php echo ($selected_tile === 'alarms') ? 'sec-tile-visible' : 'tile-hidden' ?>">
						<span class="text-center">
							<br />
							<h4 class="text-white">Scheduled Alarms : <span class="stat-number">0</span></h4>
						</span>
					</div>
				</div>
			</div>
		</a>
		<!-- alarms div end -->
		<!-- breaks div start -->
		<a href="<?php echo site_url('breaks/index') ?>">
			<div class="col-md-12">
				<div class="panel panel-default breaks_main no-bottom-margin <?php echo ($selected_tile === 'breaks') ? '' : 'left-tile' ?>" data="breaks">
					<div class="panel-heading tile-heading">
						<h3 class="panel-title text-white"><i class="fa fa-calendar">&nbsp</i> Breaks</h3>
						<i class="fa fa-arrow-right pull-right tile-arrow">&nbsp </i>
					</div>
					<div class="panel-body breaks breaks_1 <?php echo ($selected_tile === 'breaks') ? 'tile-hidden' : 'tile-visible' ?>"></div>
					<div class="panel-body breaks_2 <?php echo ($selected_tile === 'breaks') ? 'sec-tile-visible' : 'tile-hidden' ?>">
						<span class="text-center">
							<br />
							<h4 class="text-white">Scheduled Breaks : <span class="stat-number">0</span></h4>
						</span>
					</div>
				</div>
			</div>
		</a>
		<!-- breaks div end -->
		<!-- emergency div start -->
		<a href="<?php echo site_url('emergency/index') ?>">
			<div class="col-md-12">
				<div class="panel panel-default emergencies_main no-bottom-margin <?php echo ($selected_tile === 'emergencies') ? '' : 'left-tile' ?>" data="emergencies">
					<div class="panel-heading tile-heading">
						<h3 class="panel-title text-white"><i class="fa fa-bullhorn">&nbsp</i> Emergency</h3>
						<i class="fa fa-arrow-right pull-right tile-arrow">&nbsp </i>
					</div>
					<div class="panel-body emergency emergencies_1 <?php echo ($selected_tile === 'emergencies') ? 'tile-hidden' : 'tile-visible' ?>"></div>
					<div class="panel-body emergencies_2 <?php echo ($selected_tile === 'emergencies') ? 'sec-tile-visible' : 'tile-hidden' ?>">
						<span class="text-center" class="text-white">
							<br />
							<h4 class="text-white">Emergencies : <span class="stat-number">0</span></h4>
						</span>
					</div>
				</div>
			</div>
		</a>
		<!-- emergency div end -->
	</div>
	<!--/menu bar-->
	<!--frame div-->
	<div id="right_menu" class="col-md-10 col-sm-10 right-menu">
		<?php $this->load->view($view); ?>
	</div>
	<!--/frame div-->
</div>
<!-- sidebar menu end-->
<?php $this->load->view('footer'); ?>