<div class="row" style="margin-bottom:0em">
	<!-- no devices div start -->
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading" style="background-color:#8BC34A">
				<div class="col-md-1" style="width:40px">
					<a href="<?php echo site_url('home/index') ?>">
						<i class="fa fa-arrow-left module-panel-back-arrow">&nbsp </i>
					</a>
				</div>
				<div class="col-md-3">
					<h1 class="panel-title module-panel-title"><?php echo $this->lang->line('calls'); ?></h1>
				</div>
				<div class="col-md-3">
					<h1 class="panel-title module-panel-device-name"><?php echo $this->lang->line('error_no_device_connected'); ?></h1>
				</div>
			</div>
			<div class="panel-body" style="padding:0">
				<div class="col-md-3">

				</div>
				<div class="col-md-7" style="height:510px">
					<br />
					<span class="no-dev-info-head"><b>Here you can manage the internet access of your child.</b></span><br /><br />
					<span class="no-dev-bold"><b>But you do not have any devices yet.</b></span><br /><br />
					<span class="no-dev-bold"><b>To add a device you just have to follow the following steps:</b></span><br /><br />
					<!--1-->
					<span class="badge no-dev-badge">1</span>
					<span class="no-dev-bold"><b> &nbsp&nbsp Grab your child's device</b></span><br />
					<span class="no-dev-text"> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp First you need the device you want to protect.</span><br />
					<!--2-->
					<span class="badge no-dev-badge">2</span>
					<span class="no-dev-bold"><b> &nbsp&nbsp Download Parental Values app on the device</b></span><br />
					<span class="no-dev-text"> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp On the device you want to protect, go to the Google Play Store and download Parental Values.</span><br />
					<!--3-->
					<span class="badge no-dev-badge">3</span>
					<span class="no-dev-bold"><b> &nbsp&nbsp Access the App</b></span><br />
					<span class="no-dev-text"> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Once installed, access Parental Values app, select the device type "Child", enter your<br /> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp email and password and accept the permissions.</span><br />
					<!--4-->
					<span class="badge no-dev-badge">4</span>
					<span class="no-dev-bold"><b> &nbsp&nbsp It is on your account!</b></span><br />
					<span class="no-dev-text"> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp There are no more steps. After this the device will appear linked to your account and<br /> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp you can set the configuration you wish to protect your child :)</span><br />
					
				</div>
			</div>
		</div>
	</div>
	<!-- no devices div end -->
</div>