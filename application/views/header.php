<!DOCTYPE html>
<html lang="en">
  <head>
	<!-- META SECTION -->            
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><?php echo $page_title .' | '.$this->lang->line('app_name') ?></title>
	<link rel="icon" href="favicon.ico" type="image/x-icon" />
	<!-- END META SECTION -->
	
	<!-- CSS INCLUDE -->        
	<link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url() ?>assets/css/theme-default.css"/>
	<link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url() ?>assets/css/font-awesome/css/font-awesome.min.css"/>
	<link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url() ?>assets/css/custom-tiles-styling-left-menu.css"/>
	<link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url() ?>assets/css/dropdown.css"/>
	<link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url() ?>assets/css/devices_show_div.css"/>
	<!-- EOF CSS INCLUDE -->
	<!-- VIDEO UPLOAD -->
	<link href="<?php echo base_url() ?>assets/video_upload/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url() ?>assets/video_upload/themes/explorer/theme.css" media="all" rel="stylesheet" type="text/css"/>
	<!--adding firebase api reference-->
	<script src="https://cdn.firebase.com/js/client/2.2.3/firebase.js"></script>
  
  </head>
  <body class="body-bottom-set">
	<!--top bar-->
	<div class="row top-bar-style" style="">
		<nav class="navbar navbar-default">
			<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#" style="font-size:25px">Parental Values</a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">				  
					<ul class="nav navbar-nav navbar-right">
						<li class="nav-item" style="margin-right:50px">
							<img class="circle" src="<?php echo base_url() ?>assets/img/user.jpg" style="border:2px solid white;border-radius: 50%;height:100%;width:60px" alt="...">
							<span id="user_name">User Name</span>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="font-size:15px">Notifications 
								<span class="fa fa-bell" style="margin-left:5px"></span>
							</a>
							<ul class="dropdown-menu">
								<li><a href="#"> Notification 1</a></li>
								<li><a href="#"> Notification 2</a></li>
								<li><a href="#"> Notification 3</a></li>
								<li><a href="#"> Notification 4</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="font-size:15px">My Account <span class="fa fa-ellipsis-v" style="margin-left:5px"></span></a>
							<ul class="dropdown-menu">
								<li><a href="#"> <i class="fa fa-gear" style="margin-right:10px"> </i> Settings</a></li>
								<li><a href="<?php echo site_url('multimedia/index') ?>" onClick="bring_multimedia()"> <i class="fa fa-video-camera" style="margin-right:10px"> </i> Multimedia</a></li>
								<li><a href="#"> <i class="fa fa-user" style="margin-right:10px"> </i> Profile</a></li>
								<li><a href="#"> <i class="fa fa-question-circle" style="margin-right:10px"> </i> Help</a></li>
								<li role="separator" class="divider"></li>
								<li><a href="#"> <i class="fa fa-sign-out" style="margin-right:10px"> </i> Log Out</a></li>
							</ul>
						</li>
					</ul>
				</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>			
	</div>