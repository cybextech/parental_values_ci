<!-- START PLUGINS -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins/jquery/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins/bootstrap/bootstrap.min.js"></script>  
<!-- END PLUGINS -->

<!-- THIS PAGE PLUGINS -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins/nvd3/lib/d3.v3.js"></script>        
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins/nvd3/nv.d3.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
<!-- END PAGE PLUGINS -->         

<!-- START TEMPLATE -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins.js"></script>        
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/actions.js"></script>
<!-- CK EDITOR -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/ckeditor/ckeditor.js"></script>
<!-- END TEMPLATE -->
<script type="text/javascript" src="<?php echo base_url()?>assets/js/custom-control-tiles-hover.js"></script>
<!-- VIDEO UPLOAD -->
<script src="<?php echo base_url()?>assets/video_upload/js/plugins/sortable.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/video_upload/js/fileinput.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/video_upload/js/locales/fr.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/video_upload/js/locales/es.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/video_upload/themes/explorer/theme.js" type="text/javascript"></script>
<?php
	$base_url_firebase = $this->session->userdata('firebase_base_url');
	$user_id = $this->session->userdata('user_id');
	$device_id = $this->session->userdata('device_id');
?>
<script>
//raw vals
var fb_url = "<?php echo $base_url_firebase; ?>";
var fb_user_id = "<?php echo $user_id; ?>";
var fb_dev_id = "<?php echo $device_id ?>";

//general links
var site_url = "<?php echo site_url(); ?>";
var base_url = "<?php echo base_url(); ?>";
</script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/firebase/custom-bring-counts.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/firebase/custom-devices-bringer.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/firebase/custom-internet-controller.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/firebase/custom-applications-controller.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/firebase/custom-calls-controller.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/firebase/custom-multimedia-controller.js"></script>
</body>
</html>