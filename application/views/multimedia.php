<div class="row" style="margin-bottom:0em">
	<!-- no devices div start -->
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading" style="background-color:#74C374">
				<div class="col-md-1" style="width:40px">
					<a href="dash.php">
						<i class="fa fa-arrow-left module-panel-back-arrow">&nbsp </i>
					</a>
				</div>
				<div class="col-md-3">
					<h1 class="panel-title module-panel-title"><?php echo $this->lang->line('multimedia'); ?></h1>
				</div>
			</div>
			<div class="panel-body" style="padding:0">
				<!-- START JUSTIFIED TABS -->
				<div class="panel panel-default tabs">
					<ul class="nav nav-tabs nav-justified">
						<li class="active"><a href="#tab1" data-toggle="tab">Upload</a></li>
						<li><a href="#tab2" data-toggle="tab">View</a></li>
					</ul>
					<div class="panel-body tab-content">
						<!--tab 1 content-->
						<div class="tab-pane active list-group list-group-contacts scroll" style="height:100%" id="tab1">
							<!--tabs for upload video, images and text-->
							<div class="panel panel-default tabs">
								<ul class="nav nav-tabs nav-justified">
									<li class="active"><a href="#upload_vid" data-toggle="tab">Upload Videos</a></li>
									<li><a href="#upload_img" data-toggle="tab">Upload Images</a></li>
									<li><a href="#upload_txt" data-toggle="tab">Upload Text</a></li>
								</ul>
								<div class="panel-body tab-content">
									<!--upload video content tab-->
									<div class="tab-pane active list-group list-group-contacts scroll" style="height:100%" id="upload_vid">
										<p>For Video upload, 3gp, mp4 and mkv file types are supported</p><br />
										<div class="form-group">
											<label for="vid_caption">Caption for video(s)</label>
											<input type="text" class="form-control" name="vid_caption" id="vid_caption">
										</div>
										<div class="form-group">
											<label for="vid_description">Description for video(s)</label>
											<textarea type="text" class="form-control" name="vid_description" id="vid_description"></textarea>
										</div>
										<form enctype="multipart/form-data">
											<div class="form-group">
												<label for="vid_upload">File(s) here</label>
												<input id="vid_upload" type="file" multiple class="file" data-allowed-file-extensions='["3gp" ,"mp4", "mkv"]'
												data-upload-url="<?php echo site_url(); ?>/multimedia/handle_multimedia">
											</div>
										</form>
									</div>
									<!--upload image content tab-->
									<div class="tab-pane list-group list-group-contacts scroll" style="height:100%" id="upload_img">
										<p>For Image upload, jpg, jpeg, png and bmp file types are supported</p><br />
										<div class="form-group">
											<label for="img_caption">Caption for image(s)</label>
											<input type="text" class="form-control" name="img_caption" id="img_caption">
										</div>
										<div class="form-group">
											<label for="img_description">Description for image(s)</label>
											<textarea type="text" class="form-control" name="img_description" id="img_description"></textarea>
										</div>
										<form enctype="multipart/form-data">
											<div class="form-group">
												<label for="img_upload">File(s) here</label>
												<input id="img_upload" type="file" multiple class="file" data-allowed-file-extensions='["jpg" ,"jpeg", "png", "bmp"]'
												data-upload-url="<?php echo site_url(); ?>/multimedia/handle_multimedia">
											</div>
										</form>
									</div>
									<!--upload text content tab-->
									<div class="tab-pane list-group list-group-contacts scroll" style="height:100%;width:100%" id="upload_txt">
										<div class="form-group">
											<label for="text_subject">Subject</label>
											<input type="text" class="form-control" name="text_subject" id="text_subject">
										</div>
										<div class="form-group">
											<label for="multimedia_text">Text Content Here</label>
											<textarea class="ckeditor" name="multimedia_text" id="multimedia_text"></textarea>
										</div>
										<button type="submit" class="btn btn-primary" id="text_save_btn" onClick="save_text()">Save</button>
									</div>
								</div>
							</div>
						</div>
						<!--tab 2 content-->
						<div class="tab-pane scroll" style="height:100%" id="tab2">
							<!--tabs for viewing video, images and text-->
							<div class="panel panel-default tabs">
								<ul class="nav nav-tabs nav-justified">
									<li class="active"><a href="#view_vid" data-toggle="tab">View Videos</a></li>
									<li><a href="#view_img" data-toggle="tab">View Images</a></li>
									<li><a href="#view_txt" data-toggle="tab">View Text</a></li>
								</ul>
								<div class="panel-body tab-content">
									<!--view video content tab-->
									<div class="tab-pane active list-group list-group-contacts scroll" style="height:430px" id="view_vid">
										<div class="row">
											<div class="col-md-3 col-sm-3">
												<video id="my-video" class="video-js" controls preload="auto" width="340px" height="200px"
												  poster="video/f49f997b23565557ecd43f81451e989f.jpg" data-setup="{}">
													<source src="video/f49f997b23565557ecd43f81451e989f.mp4" type='video/mp4'>
													<p class="vjs-no-js">
													  To view this video please enable JavaScript, and consider upgrading to a web browser that
													  <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
													</p>
												  </video>
											</div>
											<div class="col-md-3 col-sm-3">
												<video id="my-video" class="video-js" controls preload="auto" width="340px" height="200px"
												  poster="video/f49f997b23565557ecd43f81451e989f.jpg" data-setup="{}">
													<source src="video/f49f997b23565557ecd43f81451e989f.mp4" type='video/mp4'>
													<p class="vjs-no-js">
													  To view this video please enable JavaScript, and consider upgrading to a web browser that
													  <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
													</p>
												  </video>
											</div>
											<div class="col-md-3 col-sm-3">
												<video id="my-video" class="video-js" controls preload="auto" width="340px" height="200px"
												  poster="video/f49f997b23565557ecd43f81451e989f.jpg" data-setup="{}">
													<source src="video/f49f997b23565557ecd43f81451e989f.mp4" type='video/mp4'>
													<p class="vjs-no-js">
													  To view this video please enable JavaScript, and consider upgrading to a web browser that
													  <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
													</p>
												</video>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>       												
					<!-- END JUSTIFIED TABS -->
				</div>
			</div>
		</div>
		<!-- no devices div end -->
	</div>
	<!-- first divs row end -->			
</div>