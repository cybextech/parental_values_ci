<div class="row" style="margin-bottom:0em">
	<!-- connected div start -->
	<div class="col-md-12">
		<div class="panel panel-default" style="height:570px">
			<div class="panel-heading" style="background-color:#3F51B5">
				<div class="col-md-1" style="width:40px">
					<a href="<?php echo site_url('home/index') ?>">
						<i class="fa fa-arrow-left" style="color:white;font-size:20px;padding-top:5px">&nbsp </i>
					</a>
				</div>
				<div class="col-md-3">
					<h1 class="panel-title" style="color:white;font-size:28px"><?php echo $this->lang->line('internet'); ?></h1>
				</div>
				<div class="col-md-3">
					<h1 class="panel-title" style="color:white;font-size:20px;font-weight:bold"><?php echo urldecode($device_name) ?></h1>
				</div>
			</div>
			<div class="panel-body" style="padding:0">
				<div class="col-md-9">
					<div class="panel-body" style="padding:0">
						<div class="panel-heading" style="background-color:#F7F7F7">
							<h1 class="panel-title" style="color:black;font-size:18px"> Choose which contents you want to disable with Parental Values</h1>
						</div>
						<div class="panel-body scroll" style="height:450px">                                
							<?php echo $internet_tabs; ?>                              
						</div>
					</div>
				</div>
				<div class="col-md-3" style="">
					<div class="panel-heading" style="background-color:#303F9E">
						<h1 class="panel-title" style="color:white;font-size:20px"> Configuration</h1>
					</div>
					<div class="panel-body" style="background-color:#ECECEC;height:450px">
						<br />
						<span style="color:grey;font-size:14px;margin-left:5%;font-weight:bold">Which control type you want to use?</span><br /><br />
						<div class="form-group">
							<label class="col-md-5" style="margin-top:5px">
								<span style="color:grey;margin-left:5%;font-size:13px">BLOCK MODE</span>
							</label>
							<div class="col-md-7">                                        
								<select class="form-control">
									<option value="1">White List</option>
									<option value="2" selected>Filters</option>
								</select>
							</div>
						</div><br />
						<hr style="border-top:1px dotted;color:grey;width:100%;margin-top:8px;margin-bottom:7px">
						
						<span style="color:grey;font-size:14px;margin-left:5%;font-weight:bold">Which security level you want to<br />&nbsp&nbsp&nbsp acivate?</span><br /><br />
						<div class="form-group">
							<label class="col-md-7" style="margin-top:5px">
								<span style="color:grey;margin-left:5%;font-size:13px">SECURITY LEVEL</span>
							</label>
							<div class="col-md-3 pull-right">                                        
								<select class="form-control">
									<option value="1">1</option>
									<option value="2">2</option>
								</select>
							</div>
						</div><br />
						<hr style="border-top:1px dotted;color:grey;width:100%;margin-top:5px;margin-bottom:7px">	
						<span style="color:grey;font-size:14px;margin-left:5%;font-weight:bold">Do you want to use safe search in<br />&nbsp&nbsp&nbsp  Google?</span><br /><br />
						<div class="form-group">
							<label class="col-md-7" style="margin-top:5px">
								<span style="color:grey;margin-left:5%;font-size:13px">SAFE SEARCH</span>
							</label>
							<div class="col-md-3 pull-right">                                        
								<label class="switch pull-right" style="margin-right:3%">
									<input style="color:white" type="checkbox" class="switch" value="1"/>
									<span></span>
								</label>
							</div>
						</div><br />
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- connected div end -->
</div>