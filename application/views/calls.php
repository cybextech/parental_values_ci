<div class="row" style="margin-bottom:0em">
	<!-- connected div start -->
	<div class="col-md-12">
		<div class="panel panel-default" style="height:570px">
			<div class="panel-heading" style="background-color:#8BC34A">
				<div class="col-md-1" style="width:40px">
					<a href="<?php echo site_url('home/index') ?>">
						<i class="fa fa-arrow-left module-panel-back-arrow">&nbsp </i>
					</a>
				</div>
				<div class="col-md-3">
					<h1 class="panel-title module-panel-title"><?php echo $this->lang->line('calls'); ?></h1>
				</div>
				<div class="col-md-3">
					<h1 class="panel-title module-panel-device-name"><?php echo urldecode($device_name) ?></h1>
				</div>
			</div>
			<div class="panel-body" style="padding:0">
				<div class="col-md-9">
					<div class="panel-body" style="padding:0">
						<div class="panel-heading" style="background-color:#F7F7F7">
							<h1 class="panel-title" style="color:black;font-size:18px"> Calls Activity Controller</h1>
						</div>
						<?php 
							echo $calls_n_contacts_tabs;
						?>											
					</div>
				</div>
				<div class="col-md-3" style="">
					<div class="panel-heading" style="background-color:#689F38">
						<h1 class="panel-title" style="color:white;font-size:20px"> Configuration</h1>
					</div>
					<div class="panel-body" style="background-color:#ECECEC;height:450px">
						<br />
						<span style="color:grey;font-size:14px;margin-left:5%;font-weight:bold">Do you want to block the calls from<br />&nbsp&nbsp&nbsp unknown numbers?</span><br /><br />
						<span style="color:grey;margin-left:5%;font-size:13px">BLOCK UNKNOWN CALLS</span>
						<label class="switch pull-right" style="margin-right:3%">
							<input style="color:white" type="checkbox" class="switch" value="1"/>
							<span></span>
						</label> 
						<hr style="border-top:1px dotted;color:grey;width:100%;margin-top:5px;margin-bottom:7px">
						<br />
						<span style="color:grey;font-size:14px;margin-left:5%;font-weight:bold">Do you want to block the calls from<br />&nbsp&nbsp&nbsp international numbers?</span><br /><br />
						<span style="color:grey;margin-left:5%;font-size:13px">BLOCK INTERNATIONAL CALLS</span>
						<label class="switch pull-right" style="margin-right:3%">
							<input style="color:white" type="checkbox" class="switch" value="1"/>
							<span></span>
						</label> 
						<hr style="border-top:1px dotted;color:grey;width:100%;margin-top:5px;margin-bottom:7px">	
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- connected div end -->
</div>