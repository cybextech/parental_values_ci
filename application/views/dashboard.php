<!-- first divs row -->
<div class="row scroll" style="margin-bottom:0px">
	<!--left div for stat and geo loc-->
	<div class="col-md-9">
		<!--div for statistics-->
		<div class="row">
			<!-- statistics div start -->
			<a href="statistics.php">
				<div class="panel panel-default stat_main left-tile" style="height:402px;margin-bottom:5px" data="stat">
					<div class="panel-heading" style="background-color:#607D8B">
						<h3 class="panel-title" style="color:white"><i class="fa fa-line-chart">&nbsp</i> Statistics</h3>
						<i class="fa fa-arrow-right pull-right tile-arrow">&nbsp </i>
					</div>
					<div class="panel-body stat_1" style="padding:0">
						<div id="chart-1" style="height: 350px;"><svg></svg></div>
					</div>
					<div class="panel-body stat_2" style="background-color:#607D8B;padding:0;display:none;height:350px">
						<div class="row">
							<div class="col-md-6">
								<br /><span style="font-size:16px;color:white">&nbsp&nbsp&nbsp Most used applications</span>
								<hr style="color:white;width:90%;margin-top:5px;margin-bottom:7px">
								<div class="row">
									<div class="col-md-2">
										<img src="<?php echo base_url() ?>assets/img/icons/whatsapp_icon.png" style="margin-left:5px;height:45px;width:65px">
									</div>
									<div class="col-md-10">
										<span style="font-size:20px;color:white;"> WhatsApp Massenger</span>
									</div>
								</div>
								<hr style="border-top:1px dotted;color:white;width:90%;margin-top:5px;margin-bottom:7px">
								<div class="row">
									<div class="col-md-2">
										<img src="<?php echo base_url() ?>assets/img/icons/gallery_icon.png" style="margin-left:15px;height:35px;width:35px">
									</div>
									<div class="col-md-10">
										<span style="font-size:20px;color:white;"> Gallery</span>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<br /><br /><span style="font-size:20px;color:white">Last 24h usage:</span>
								<br /><br /><span style="font-size:35px;color:white;font-weight:bold">0h 9m 30s</span>
							</div>
						</div>
					</div>
				</div>
			</a>
			<!-- statistics div end -->
		</div>
		<!--div for geo loc-->
		<div class="row">
			<!-- geolocation div start -->
			<a href="geolocation.php">
				<div class="panel panel-default geo_main left-tile" style="height:402px;margin-bottom:0px" data="geo">
					<div class="panel-heading" style="background-color:#607D8B">
						<h3 class="panel-title" style="color:white"><i class="fa fa-map-marker">&nbsp</i> Geolocation</h3>
						<i class="fa fa-arrow-right pull-right tile-arrow">&nbsp </i>
					</div>
					<div class="panel-body geo_1" style="padding:0">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3402.8190686135936!2d74.35595991462094!3d31.474162956572606!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39190680725a6e33%3A0x1c1a0d21d0e779bd!2sCybextech!5e0!3m2!1sen!2s!4v1494505561497" width="100%" height="346" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
					<div class="panel-body geo_2" style="background-color:#607D8B;padding:0;display:none;height:350px">
						<br /><span style="font-size:16px;color:white">&nbsp&nbsp&nbsp Locations where the devices are:</span>
						<hr style="color:white;width:92%;margin-top:5px;margin-bottom:7px">
						<div class="row">
							<div class="col-md-2">
								&nbsp&nbsp&nbsp&nbsp <span class="badge" style="padding:6px 12px;font-size:14px;background-color:grey;border-radius:50%">1</span>
							</div>
							<div class="col-md-2">
								<span><i class="fa fa-mobile" style="font-size:85px;color:white"></i></span>
							</div>
							<div class="col-md-8">
								<br />
								<span style="font-size:15px;color:white;font-weight:bold">GT-I9105P</span><br />
								<span style="font-size:13px;color:white;">Al-Noor Town, Lahore, Pakistan</span><br />
								<span style="font-size:13px;color:white;">16/05/2017 02:15 PM</span>
							</div>
						</div>
						<hr style="color:white;width:92%;margin-top:5px;margin-bottom:7px">	
					</div>
				</div>
			</a>
			<!-- geolocation div end -->
		</div>
	</div>
	<!--right div for devices-->
	<div class="col-md-3">
		<!-- device div start -->
		<div class="panel panel-default">
			<a href="devices.php">
				<div class="panel-heading" style="background-color:#607D8B">
					<h3 class="panel-title" style="color:white"><i class="fa fa-mobile">&nbsp</i> Devices</h3>
					<i class="fa fa-arrow-right pull-right" style="color:white;font-size:15px;padding-top:7px">&nbsp </i>
				</div>
			</a>
			<div id="devices" class="panel-body" style="padding:0;height:755px;overflow:auto">
				<div class="text-center">
					<span id="device_fetcher_spinner"><br /><img src="<?php echo base_url() ?>assets/img/spinner.gif" style="height:18px;width:18px"> Bringing Devices...</span>
				</div>
			</div>
		</div>
		<!-- device div end -->
	</div>					
</div>
<!-- first divs row end -->