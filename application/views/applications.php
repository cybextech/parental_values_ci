<div class="row" style="margin-bottom:0em">
	<!-- connected div start -->
	<div class="col-md-12">
		<div class="panel panel-default" style="height:570px">
			<div class="panel-heading" style="background-color:#9C26B0">
				<div class="col-md-1" style="width:40px">
					<a href="<?php echo site_url('home/index') ?>">
						<i class="fa fa-arrow-left" style="color:white;font-size:20px;padding-top:5px">&nbsp </i>
					</a>
				</div>
				<div class="col-md-3">
					<h1 class="panel-title" style="color:white;font-size:28px"><?php echo $this->lang->line('applications'); ?></h1>
				</div>
				<div class="col-md-3">
					<h1 class="panel-title" style="color:white;font-size:20px;font-weight:bold"><?php echo urldecode($device_name) ?></h1>
				</div>
			</div>
			<div class="panel-body" style="padding:0">
				<div class="col-md-9">
					<div class="panel-body" style="padding:0;padding-bottom:0px">
						<div class="panel-heading" style="background-color:#F7F7F7">
							<h1 class="panel-title" style="color:black;font-size:18px"> Choose which contents you want to disable with Parental Values</h1>
						</div>
						<div class="panel-body scroll" style="height:450px">                                
							<?php
								//showing applications with tabs
								echo $applications_tabs;
							?>                            
						</div>
					</div>
				</div>
				<div class="col-md-3" style="">
					<div class="panel-heading" style="background-color:#7B1EA2">
						<h1 class="panel-title" style="color:white;font-size:20px"> Configuration</h1>
					</div>
					<div class="panel-body" style="background-color:#ECECEC;height:450px">	
						<span style="color:grey;font-size:14px;margin-left:5%;font-weight:bold">Do you want to lock the new installed<br />&nbsp&nbsp&nbsp apps until you allow its access?</span><br /><br />
						<div class="form-group">
							<label class="col-md-8" style="margin-top:5px">
								<span style="color:grey;margin-left:5%;font-size:13px">LOCK NEW APPLICATIONS</span>
							</label>
							<div class="col-md-3 pull-right">                                        
								<label class="switch pull-right" style="margin-right:3%;margin-top:5%">
									<input style="color:white" type="checkbox" class="switch" value="1"/>
									<span></span>
								</label>
							</div>
						</div><br /><br />
						<hr style="border-top:1px dotted;color:grey;width:100%;margin-top:5px;margin-bottom:7px">
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- connected div end -->
</div>