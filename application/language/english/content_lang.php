<?php 
$lang['app_name'] = 'Parental Values';

//page names
$lang['home'] = 'Home';
$lang['internet'] = 'Internet';
$lang['applications'] = 'Applications';
$lang['calls'] = 'Calls';
$lang['multimedia'] = 'Multimedia';
//validation messages
$lang['validation_required'] = 'is required';
$lang['validation_invalid_email'] = 'Invalid email address';
//field names
$lang['validation_username_email'] = 'Username/Email';
$lang['validation_email_address'] = 'Email address';
$lang['validation_username'] = 'Username';
$lang['validation_password'] = 'Password';
$lang['validation_confirm_password'] = 'Confirm password';
$lang['validation_category'] = 'Category';
$lang['validation_city'] = 'City';
$lang['validation_district'] = 'District';
$lang['validation_product_name'] = "Prodcut Name";
$lang['validation_product_category'] = "Prodcut Category";
$lang['validation_product_city'] = 'Product City';
$lang['validation_radius_name'] = 'Radius Name';
$lang['validation_longitude'] = 'Longitude';
$lang['validation_latitude'] = 'Latitude';
$lang['validation_length'] = 'Radius Length';
$lang['validation_product'] = 'Product';

//js validation messages
$lang['validation_username_exist'] = 'This username already exist';
$lang['validation_email_exist'] = 'This email address already exist';
$lang['validation_reenter_password'] = 'Renter your password to confirm';
$lang['validation_password_not_same'] = 'Passwords are not same';
$lang['validattion_invalid_current_password'] = 'Current password is invalid';
$lang['validation_invalid_file_type'] = 'Invalid file type';

//boy login messages
$lang['incorrect_boy_phone'] = 'Invalid phone number';
$lang['incorrect_boy_passcode'] = 'Invalid password';
$lang['boy_login_success'] = 'Login successful';

//head navigation
$lang['nav_change_password'] = 'Change Password';
$lang['nav_logout'] = 'Log Out';
$lang['nav_admins'] = 'Admins';
$lang['nav_users'] = 'Users';
$lang['nav_manage'] = 'Manage';
$lang['nav_create'] = 'Create';
$lang['nav_new_orders'] = 'New Orders';
$lang['nav_cancelled'] = 'Cancelled';
$lang['nav_dashboard'] = 'Dashboard';
$lang['nav_categories'] = 'Categories';
$lang['nav_locations'] = 'Locations';
$lang['nav_create_city'] = 'Create City';
$lang['nav_manage_cities'] = 'Manage Cities';
$lang['nav_create_district'] = 'Create District';
$lang['nav_manage_districts'] = 'Manage Districts';
$lang['nav_products'] = 'Products';
$lang['nav_push_notifications'] = 'Push Notifications';
$lang['nav_geo_fence'] = 'Geo Fencing';
$lang['nav_response'] = 'Messages';
$lang['nav_create_radius'] = 'Create Radius';
$lang['nav_manage_radius'] = 'Manage Radius';
$lang['nav_track_boys'] = 'Track Delivery Boys';
$lang['nav_notifications'] = 'Notifications';
$lang['nav_connect_radius'] = 'Connect Radius';

$lang['new_response_from'] = "New Message From ";
//Login Page
$lang['login_title'] = 'Login';
$lang['login_heading'] = 'Admin - Login';
$lang['login_placeholder_username'] = 'Username';
$lang['login_placeholder_password'] = 'Password';
$lang['login_label_remember_me'] = 'Remember me';
$lang['login_button_signin'] = 'Sign in';
$lang['login_link_forgot_password'] = 'Forgot password?';

//Reset Password
$lang['reset_password_title'] = 'Reset Password';
$lang['reset_password_heading'] = 'Reset Password';
$lang['reset_password_label_current'] = "Current Password";
$lang['reset_password_label_new'] = "New Password";
$lang['reset_password_label_repeat'] = "Retype New Password";
$lang['reset_password_btn_update_pass'] = 'Update Password';
//dashboard Page
$lang['dashboard_title'] = 'Dashboard';

//create admin
$lang['create_admin_title'] = 'Create Admin';
$lang['create_admin_heading'] = 'Create Admin';
$lang['create_admin_label_username'] = 'Username';
$lang['create_admin_label_password'] = 'Password';
$lang['create_admin_label_confirm_password'] = 'Confirm Password';
$lang['create_admin_label_can_add_user'] = 'Create User';
$lang['create_admin_label_can_edit_user'] = 'Edit User';
$lang['create_admin_label_can_delete_user'] = 'Delete User';
$lang['create_admin_btn_add'] = "Create Admin";

$lang['edit_admin_title'] = 'Edit Admin';
$lang['edit_admin_heading'] = 'Edit Admin';
$lang['edit_admin_btn_update'] = "Update Admin";
//manage admins
$lang['manage_admins_title'] = "Manage Admins";
$lang['manage_admins_heading'] = "Manage Admins";
$lang['manage_admins_btn_create'] = "Create Admin";

//users

$lang['create_user_title'] = 'Create User';
$lang['create_user_heading'] = 'Create User';
$lang['create_user_btn_add'] = 'Create User';
$lang['create_user_label_username'] = 'Username';
$lang['create_user_label_password'] = 'Password';
$lang['create_user_label_confirm_password'] = 'Confirm Password';
$lang['create_user_label_phone'] = 'Telephone';
$lang['create_user_label_email'] = 'Email';
$lang['create_user_label_website'] = 'Website';
$lang['create_user_label_fb_link'] = 'Facebook Link';
$lang['create_user_label_app_name'] = 'App Name';
$lang['create_user_label_andriod'] = 'Andriod Icon';
$lang['create_user_label_ios'] = 'Ios Icon';
$lang['create_user_label_andriod_version'] = 'Andriod App Version';
$lang['create_user_label_ios_version'] = 'Ios App Version';
$lang['create_user_label_shopping_cart'] = 'Activate Shopping Cart';
$lang['create_user_label_expiry_date'] = 'Subscription Expiry Date';
$lang['create_user_label_push_notification'] = 'Push Notifications';
$lang['create_user_label_geo_fencing'] = 'Geo Fencing';
$lang['create_user_label_delivery_cost'] = 'Delivery Cost';
$lang['create_user_label_cat_home_screeen'] = 'Activate Category Home Screen';
$lang['create_user_label_contact_name'] = 'Contact Name';
$lang['create_user_label_status'] = 'Status';
$lang['create_user_label_product_specs'] = 'Product Custom Specs';
$lang['user_status_active'] = 'Active';
$lang['user_status_inactive'] = 'Inactive';

//edit user
$lang['edit_user_title'] = 'Edit User';
$lang['edit_user_heading'] = 'Edit User';
$lang['edit_user_btn_add'] = 'Update User';
//manage users
//manage admins
$lang['manage_users_title'] = "Manage Users";
$lang['manage_users_heading'] = "Manage Users";
$lang['manage_orders_heading'] = "Manage Orders";
$lang['manage_users_btn_create'] = "Create User";
//user login
$lang['user_login_heading'] = 'User - Login';
//create category
$lang['create_category_title'] = 'Create New Category';
$lang['create_category_heading'] = 'Create New Category';
$lang['create_category_btn_add'] = 'Create Category';
$lang['category_label_category_name'] = 'Category Name';
$lang['category_label_category_parent_name'] = 'Select Parent Category';
$lang['category_label_category_thumbnail'] = 'Thumbnail';
$lang['category_type_parent'] = 'Parent';
$lang['category_type_sub'] = 'Sub Category';
//edit categroy
$lang['edit_category_title'] = 'Edit Category';
$lang['edit_category_heading'] = 'Edit Category';
$lang['edit_category_btn_add'] = 'Update Category';
//manage categories
$lang['manage_categories_title'] = 'Manage Categories';
$lang['manage_categories_heading'] = 'Manage Categories';
$lang['manage_categories_btn_add'] = 'New Category';
//cities
$lang['create_city_title'] = 'Create City';
$lang['create_city_heading'] = 'Create City';
$lang['create_city_btn_add'] = 'Create City';
$lang['edit_city_title'] = 'Edit City';
$lang['edit_city_heading'] = 'Edit City';
$lang['edit_city_btn_add'] = 'Update City';
$lang['label_city_name'] = 'City Name';
$lang['manage_cities_title'] = 'Manage Cities';
$lang['manage_cities_heading'] = 'Manage Cities';
$lang['manage_cities_btn_add'] = 'New City';
//districts
$lang['create_district_title'] = 'Create District';
$lang['create_district_heading'] = 'Create District';
$lang['create_district_btn_add'] = 'Create District';
$lang['edit_district_title'] = 'Edit District';
$lang['edit_district_heading'] = 'Edit District';
$lang['edit_district_btn_add'] = 'Update District';
$lang['label_district_name'] = 'District Name';
$lang['manage_districts_title'] = 'Manage Districts';
$lang['manage_districts_heading'] = 'Manage Districts';
$lang['manage_districts_btn_add'] = 'New District';

// Products
$lang['create_product_title'] = 'Create Product';
$lang['create_product_heading'] = 'Create Product';
$lang['create_product_btn_add'] = 'Create Product';
$lang['label_product_name'] = 'Product Name';
$lang['label_product_category'] = 'Select Category';
$lang['label_product_city'] = 'Select City';
$lang['label_product_district'] = 'Select District';
$lang['label_product_sales_phone'] = 'Sales Phone';
$lang['label_product_ivr_enabled'] = 'IVR Enabled';
$lang['label_product_sales_email'] = 'Sales Email';
$lang['label_product_price_before_discount'] = 'Price Before Discount';
$lang['label_product_discount'] = 'Price Discount(%)';
$lang['label_product_tax'] = 'Tax(%)';
$lang['label_product_delivery_cost'] = 'Delivery Cost';
$lang['label_product_note'] = 'Product Note';
$lang['label_product_slider'] = 'Show Product in Slider';
$lang['label_product_offline'] = 'Available offline';
$lang['label_product_currency_symbol'] = 'Price Currency Symbol';
$lang['label_product_images'] = 'Product Images';
$lang['edit_product_title'] = 'Edit Product';
$lang['edit_product_heading'] = 'Edit Product';
$lang['edit_product_btn_add'] = 'Update Product';
$lang['manage_products_title'] = 'Manage Products';
$lang['manage_products_heading'] = 'Manage Products';
$lang['manage_products_btn_add'] = 'New Product';

$lang['product_select_parent_category']  = 'Select parent category';
$lang['product_select_sub_category']  = 'Select sub category';
$lang['product_currency_egyptian_pound'] = '£ Egyptian pound';
$lang['product_currency_usd'] = '$ USD';
//notifications
$lang['manage_notifications_title'] = 'Push Notifications';
$lang['manage_notifications_heading'] = 'Push Notifications';
//responses
$lang['manage_response_title'] = 'Manage Messages';
$lang['manage_response_heading'] = 'Manage Messages';
$lang['manage_response_label_from'] = 'From';
$lang['manage_response_label_to'] = 'To';
//create city
$lang['create_radius_title'] = 'Create Radius';
$lang['create_radius_heading'] = 'Create Radius';
$lang['create_radius_btn_add'] = 'Create Radius';
$lang['label_radius_name'] = 'Radius Name';
$lang['label_longitude'] = 'Longitude';
$lang['label_latitude'] = 'Latitude';
$lang['label_radius_length'] = 'Radius Length(KM)';
$lang['radius_select_search'] = 'Search and select product for radius';
$lang['radius_select_category'] = 'Select Category';
$lang['label_search_product'] = 'Search Product';
$lang['label_search_product_name_note'] = 'Search Product Name/Note';
$lang['label_select_product'] = 'Select Product';
$lang['create_radius_btn_search'] = 'Search Products';
$lang['edit_radius_title'] = 'Edit Radius';
$lang['edit_radius_heading'] = 'Edit Radius';
$lang['edit_radius_btn_add'] = 'Update Radius';
$lang['manage_radius_title'] = 'Manage Radius';
$lang['track_boys_title'] = 'Track Delivery Boys';
$lang['manage_radius_heading'] = 'Manage Radius';
$lang['manage_radius_btn_add'] = 'New Radius';
//connect radius
$lang['connect_radius_title'] = 'Connect Radius';
$lang['connect_radius_heading'] = 'Connect Radius';
$lang['radius_select_city'] = "Select City";
$lang['radius_select_district'] = "Select District";
//product radius
$lang['product_radius_title'] = "Manage Product Radius";
$lang['product_radius_heading'] = "Manage Product Radius";
$lang['radius_select_radius'] = "Select Radius";
$lang['product_radius_add_radius'] = 'Connect Radius';
$lang['product_radius_remove_radius'] = 'Remove Radius';
//categroy specs

$lang['category_specs_title'] = 'Update Category Specs';
$lang['category_specs_heading'] = 'Update Category Specs';
$lang['category_specs_btn_update'] = 'Update Specs';
//dashboard
$lang['dashboard_installers'] = "Installers";
$lang['dashboard_products'] = "Products";
$lang['dashboard_sent_promotions'] = "Sent Promotions";
$lang['dashboard_users'] = "Users";
//common
$lang['label_order'] = 'Order';
$lang['label_yes'] = 'Yes';
$lang['label_no'] = 'No';
$lang['record_status_active'] = 'Active';
$lang['record_status_inactive'] = 'Inactive';
$lang['record_type_parent'] = 'Parent';
$lang['record_type_sub'] = 'Sub';
// data table
$lang['table_search_placeholder'] = 'Search';
//table headers
$lang['table_th_id'] = 'ID';
$lang['table_th_username'] = 'User Name';
$lang['table_th_add'] = 'Add';
$lang['table_th_edit'] = 'Edit';
$lang['table_th_delete'] = 'Delete';
$lang['table_th_email'] = 'Email';
$lang['table_th_status'] = 'Status';
$lang['table_th_type'] = 'Type';
$lang['table_th_app_name'] = 'App Name';
$lang['table_th_order'] = 'Order';
$lang['table_th_city'] = 'City';

$lang['table_th_created'] = 'Created On';
$lang['table_th_action'] = 'Actions';
$lang['table_th_assign_boy'] = 'Assign Boy';
$lang['table_th_assigned_boy'] = 'Assigned Boy';
$lang['table_th_name'] = 'Name';
$lang['table_th_order_name'] = 'Ordered by';
$lang['table_th_app_name'] = 'App Name';
$lang['table_th_price'] = 'Price';
$lang['table_th_discount'] = 'Discount(%)';
$lang['table_th_impressions'] = 'Impressions';
$lang['table_th_sent'] = 'Sent';
$lang['table_th_sender'] = 'Sender';
$lang['table_th_mobile'] = 'Mobile';
$lang['table_th_comment'] = 'Comment';
$lang['table_th_product_name'] = 'Product Name';
$lang['table_th_zone_name'] = 'Radius Name';
$lang['table_th_order_total'] = 'Total';
$lang['table_th_cart_details'] = 'Details';
//table action buttons
$lang['table_action_edit'] = 'Edit';
$lang['table_action_unassign'] = 'Unassign Boy';
$lang['table_action_requeue'] = 'Requeue';
$lang['table_action_delete'] = 'Delete';
$lang['table_action_cancel'] = 'Cancel';
$lang['table_action_view_as'] = 'View As';
$lang['table_action_promote'] = 'Promote';
$lang['table_action_specs'] = 'Specs';

$lang['table_action_disable'] = 'Disable';
$lang['table_action_enable'] = 'Enable';
$lang['table_action_view'] = 'View';

$lang['table_action_connect_radius'] = "Connect Radius";

$lang['table_no_records_found'] = "No Records Found";

//modals
$lang['modal_delete_header'] = 'Delete Record';
$lang['modal_cancel_order_header'] = 'Cancel Order';
$lang['modal_unassign_order_header'] = 'Unassign Order';
$lang['modal_assign_order_header'] = 'Assign Order';
$lang['modal_requeue_order_header'] = 'Requeue Order';
$lang['modal_change_status_header'] = 'Change Status';
$lang['modal_btn_close'] = 'Close';
$lang['modal_btn_unassign'] = 'Unassign';
$lang['modal_btn_requeue'] = 'Requeue';
$lang['modal_btn_cancel'] = 'Cancel';
$lang['modal_btn_delete'] = 'Delete';
$lang['modal_btn_assign'] = 'Assign';
$lang['modal_btn_inactive'] = 'Inactive';

$lang['file_upload_add_files'] = "Add Files...";
$lang['file_upload_start_upload'] = "Start Upload";
$lang['file_upload_cancel_upload'] = "Cancel Upload";
$lang['file_upload_delete'] = "Delete";
$lang['file_upload_start'] = "Start";
$lang['file_upload_cancel'] = "Cancel";
