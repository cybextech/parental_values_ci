<?php 
$lang['error_no_device_connected'] = 'No devices connected';













//error alerts
$lang['error_admin_login_error'] = 'Invalid username or password';
$lang['error_admin_login_account_expired'] = 'Subscription Expired';
$lang['error_user_login_error'] = 'Invalid username/email or password';
$lang['error_action_not_allowed'] = 'You do not have permission to perform this action';
$lang['error_login_disabled'] = 'Your account is disabled';
$lang['error_invalid_request'] = 'Invalid request';
$lang['error_something_wrong'] = 'Something goes wrong please try again.';

//success alerts
$lang['success_password_reset'] =  'Password has been changed successfully';
$lang['success_admin_created'] =  'Admin created successfully';
$lang['success_admin_updated'] =  'Admin updated successfully';
$lang['success_admin_deleted'] =  'Admin deleted successfully';
$lang['success_user_created'] =  'User created successfully';
$lang['success_user_updated'] =  'User updated successfully';
$lang['success_user_deleted'] =  'User deleted successfully';
$lang['success_user_disabled'] = 'User status has been changed to Inactive successfully';
$lang['success_user_enabled'] = 'User status has been changed to Active successfully';
$lang['success_category_created'] =  'Category created successfully';
$lang['success_category_updated'] =  'Category updated successfully';
$lang['success_category_deleted'] =  'Category deleted successfully';
$lang['success_category_enabled'] =  'Category status has been changed to Active successfully';
$lang['success_category_disabled'] =  'Category status has been changed to Inactive successfully';

$lang['success_city_created'] =  'City created successfully';
$lang['success_city_updated'] =  'City updated successfully';
$lang['success_city_deleted'] =  'City deleted successfully';
$lang['success_city_enabled'] =  'City status has been changed to Active successfully';
$lang['success_city_disabled'] =  'City status has been changed to Inactive successfully';

$lang['success_branch_activated'] =  'Branch status changed to Active successfully';
$lang['success_branch_deactivated'] =  'Branch status changed to Inactive successfully';

$lang['success_district_created'] =  'District created successfully';
$lang['success_district_updated'] =  'District updated successfully';
$lang['success_district_deleted'] =  'District deleted successfully';
$lang['success_district_enabled'] =  'District status has been changed to Active successfully';
$lang['success_district_disabled'] =  'District status has been changed to Inactive successfully';

$lang['success_product_created'] =  'Product created successfully';
$lang['success_product_updated'] =  'Product updated successfully';
$lang['success_product_deleted'] =  'Product deleted successfully';
$lang['success_product_enabled'] =  'Product status has been changed to Active successfully';
$lang['success_product_disabled'] =  'Product status has been changed to Inactive successfully';

$lang['success_radius_created'] =  'Radius created successfully';
$lang['success_radius_updated'] =  'Radius updated successfully';
$lang['success_radius_deleted'] =  'Radius deleted successfully';
$lang['success_radius_enabled'] =  'Radius status has been changed to Active successfully';
$lang['success_radius_disabled'] =  'Radius status has been changed to Inactive successfully';

$lang['success_notifications_sent'] = 'Notification sent successfully';
$lang['success_responses_deleted'] = 'Message deleted successfully';

$lang['success_order_changed'] = 'Order changed successfully';

$lang['info_no_unseen_notifcations'] = 'No unseen messages';

$lang['success_product_radius_added'] = "Radius connected with product successfully";
$lang['success_product_radius_removed'] = "Radius removed from product successfully";
$lang['success_category_specs_updated'] = "Category specs updated successfully";
//modals text

$lang['modal_delete_admin'] = 'Do you want to delete this admin? confirm!';
$lang['modal_delete_user'] = 'Are you sure you want to delete this user?';
$lang['modal_disable_user'] = 'Are you sure you want to "Inactive" this user';
$lang['modal_delete_category'] = 'Are you sure you want to delete all products inside this category?';
$lang['modal_disable_category'] = 'Are you sure you want to hide all products inside this category';
$lang['modal_delete_city'] = 'Are you sure you want to delete all products inside this city?';
$lang['modal_disable_city'] = 'Are you sure you want to hide all products inside this city';
$lang['modal_delete_district'] = 'Are you sure you want to delete all products inside this district?';
$lang['modal_disable_district'] = 'Are you sure you want to hide all products inside this district';
$lang['modal_delete_product'] = 'Are you sure you want to delete this product?';
$lang['modal_disable_product'] = 'Are you sure you want to hide this product?';
$lang['modal_delete_response'] = 'Are you sure you want to delete this Message?';
$lang['modal_disable_zone'] = 'Are you sure you want to stop this Walk through notification?';
$lang['modal_delete_zone'] = 'Are you sure you want to delete this Walk through notification?';
$lang['modal_delete_record'] = 'Do you want to delete this record from user account? Confirm!';
$lang['modal_remove_radius'] = 'Do you want to remove this radius from product? Confirm!';

 ?>