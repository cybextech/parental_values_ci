<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Firebase_model extends CI_Model{
	
	protected $_firebase;
		    	
    function __construct()
    {
        parent :: __construct();
		$this->_firebase = new FirebaseLib();
    }
	
	//firebase method to bring sites against device of certain user
	function bring_internet($user_id, $device_id){
			
		$url = $user_id;
		$url = $url."/";
		$url = $url. $device_id;
		$url = $url."/internet/";
		
		return $this->_firebase->get($url);
	}
	//method to update activation status of a website
	public function update_site_status($user_id, $device_id, $site_id){
		$site_status_to_set = false;
		
		$response = $this->bring_site($user_id, $device_id, $site_id);
		$response1 = json_decode($response, true);
		
		if($response1['is_blocked'] == true){
			$site_status_to_set = false;
		}
		else{
			$site_status_to_set = true;
		}
		
		$site_status = array(
			'is_blocked' => $site_status_to_set
		);
		$url = $user_id;
		$url = $url."/";
		$url = $url. $device_id;
		$url = $url."/internet/";
		$url = $url.$site_id;
		
		$update_response = $this->_firebase->update($url, $site_status);
		return $update_response;
	}
	//get status of site in question
	public function bring_site($user_id, $device_id, $site_id){
		
		$url = $user_id;
		$url = $url."/";
		$url = $url. $device_id;
		$url = $url."/internet/";
		$url = $url.$site_id;
		
		return $this->_firebase->get($url);
	}
	
	////////////////////////////////////////////////////////////////////////////
	//bring applicaations from firebase
	public function bring_applications($user_id, $device_id){
			
		$url = $user_id;
		$url = $url."/";
		$url = $url. $device_id;
		$url = $url."/applications";
		
		return $this->_firebase->get($url);
	}
	//ajax call method to update application status
	public function update_app_status($user_id, $device_id, $app_id){
		
		$app_status_to_set = false;
		
		$response = $this->bring_application($user_id, $device_id, $app_id);
		$response1 = json_decode($response, true);
			
		if($response1['app_is_blocked'] == true){
			$app_status_to_set = false;
		}
		else{
			$app_status_to_set = true;
		}
		
		$app_status = array(
			'app_is_blocked' => $app_status_to_set
		);
		$url = $user_id;
		$url = $url."/";
		$url = $url. $device_id;
		$url = $url."/applications/";
		$url = $url.$app_id;
		
		$update_response = $this->_firebase->update($url, $app_status);
		return $update_response;
	}
	//get status of app in question
	public function bring_application($user_id, $device_id, $app_id){
		
		$url = $user_id;
		$url = $url."/";
		$url = $url. $device_id;
		$url = $url."/applications/";
		$url = $url.$app_id;
		
		return $this->_firebase->get($url);
	}
	
	////////////////////////////////////////////////////////////////////////////////////
	//method to bring contacts from firease
	public function bring_contacts($user_id, $device_id){
			
		$url = $user_id;
		$url = $url."/";
		$url = $url. $device_id;
		$url = $url."/call_register/";
		
		return $this->_firebase->get($url);
	}
	//ajax call return for updating contact status change
	public function update_contact_status($user_id, $device_id, $contact_id){
		$contact_status_to_set = false;
		
		$response = $this->bring_contact($user_id, $device_id, $contact_id);
		$response1 = json_decode($response, true);
		// return $response1['app_is_blocked'];
		// die;
		
		if($response1['is_blocked'] == true){
			$contact_status_to_set = false;
		}
		else{
			$contact_status_to_set = true;
		}
		
		$contact_status = array(
			'is_blocked' => $contact_status_to_set
		);
		$url = $user_id;
		$url = $url."/";
		$url = $url. $device_id;
		$url = $url."/call_register/contacts/";
		$url = $url.$contact_id;
		
		$update_response = $this->_firebase->update($url, $contact_status);
		return $update_response;
	}
	//get status of app in question
	public function bring_contact($user_id, $device_id, $contact_id){
		
		$url = $user_id;
		$url = $url."/";
		$url = $url. $device_id;
		$url = $url."/call_register/contacts/";
		$url = $url.$contact_id;
		
		return $this->_firebase->get($url);
	}
	
	public function save_multimedia($user_id, $media_title, $media_url, /* $media_category, */ $media_type, $is_active, $media_key, $media_caption, $media_description){
		//data array
		$multimedia_data = array(
			'media_title' => $media_title,
			'media_url' => $media_url,
			'is_active' => $is_active,
			'media_type' => $media_type,
			'media_caption' => $media_caption,
			'media_description' => $media_description,
			//'media_category' => $media_category
		);
		
		$url = $user_id;
		$url = $url."/";
		$url = $url."multimedia/".$media_type."/".$media_key."/";
		
		$response = $this->_firebase->set($url, $multimedia_data);
	}
	
	//ajax call response method for saving text in firebase 
	public function save_text($user_id, $subject, $media_type, $is_active, $media_key, $media){
		//data array
		$multimedia_data = array(
			'subject' => $subject,
			//'media_url' => $media_url,
			'is_active' => $is_active,
			'media_type' => $media_type,
			//'media_caption' => $media_caption,
			'media' => $media,
			//'media_category' => $media_category
		);
		
		$url = $user_id;
		$url = $url."/";
		$url = $url."multimedia/".$media_type."/".$media_key."/";
		
		$response = $this->_firebase->set($url, $multimedia_data);
	}
}