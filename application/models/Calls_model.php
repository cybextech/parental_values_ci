<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Calls_model extends CI_Model{
    
    function __construct()
    {
        parent :: __construct();
    }
	
	function make_call_register_n_contacts_tabs($response){
		if($response !== 'null'){
			$decoded_response = json_decode($response, true);
			//initializing tabs head
			$tab_header = '<div class="panel panel-default tabs">
							<ul class="nav nav-tabs nav-justified">
								<li class="active"><a href="#tab_contacts" data-toggle="tab">CONTACTS</a></li>
								<li><a href="#tab_calls_log" data-toggle="tab">CALLS LOG</a></li>
							</ul>
						   <div class="panel-body tab-content">';
			
			//initializing all content divs
			$tab_content_contacts = '<div class="tab-pane active list-group list-group-contacts scroll" style="height:325px" id="tab_contacts">';
			$tab_content_calls_log = '<div class="tab-pane list-group list-group-contacts scroll" style="height:325px" id="tab_calls_log">';
			
			foreach($decoded_response as $key => $register_type){
				if($key == 'call_log'){
					$tab_content_calls_log = $tab_content_calls_log.'work in progress';
				}
				elseif($key == 'contacts'){
					foreach($register_type as $key_contacts => $value_contacts){
						if($value_contacts['is_blocked'] == false){
							$block_unblock_indicator = 'checked="checked"';
						}
						else{
							$block_unblock_indicator = "&nbsp";
						}
						//echo ($value_contacts['contact_name'].'<br/>');
						$tab_content_contacts = $tab_content_contacts .'<div class="list-group-item">                                    
																		<img src="'.base_url().'assets/generic_icons/contact.png" class="pull-left" alt="'.$value_contacts['contact_name'].'"/>
																		<span class="contacts-title">'.$value_contacts['contact_name'].'</span>
																		<p>'.$value_contacts['contact_number'].'
																			<label class="switch pull-right" style="margin-right:3%">
																				<img src="'.base_url().'assets/img/spinner.gif" id="'.$key_contacts.'_load" style="height:25px;width:25px;margin-bottom:12px;display:none"></img>
																				<input style="color:white" type="checkbox" class="switch contact_dat" cat_type="" id="'.$key_contacts.'" data="'.$key_contacts.'" '.$block_unblock_indicator.'/>
																				<span></span>
																			</label>
																		</p>                                                                   
																	</div>';
					}
				}
			}
			//closing all content divs
			$tab_content_contacts = $tab_content_contacts.'</div>';
			$tab_content_calls_log = $tab_content_calls_log.'</div>';
			//concatenating all content divs with header
			$tab_header = $tab_header.$tab_content_contacts.$tab_content_calls_log;
			//closing header divs
			$tab_header = $tab_header.'</div></div>';
			$final_html = $tab_header;
			//return $response;
			return $final_html;
		}
		else{
			return 'no data found';
		}
		
	}
}