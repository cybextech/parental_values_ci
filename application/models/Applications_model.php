<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Applications_model extends CI_Model{
    
    function __construct()
    {
        parent :: __construct();
    }
	
	function make_applications_tabs($response){
		
		$response1 = json_decode($response, true);
		//initializing tabs head
		$tab_header = '<div class="panel panel-default tabs">
						<ul class="nav nav-tabs nav-justified">
							<li class="active"><a href="#tab_all_apps" data-toggle="tab">ALL</a></li>
							<li><a href="#tab_entertainment" data-toggle="tab">ENTERTAINMENT</a></li>
							<!--<li><a href="#tab_communication" data-toggle="tab">COMMUNICATION</a></li>-->
							<li><a href="#tab_multimedia" data-toggle="tab">MULTIMEDIA</a></li>
							<li><a href="#tab_connectivity" data-toggle="tab">CONNECTIVITY</a></li>
							<!--<li><a href="#tab_sports" data-toggle="tab">SPORTS</a></li>-->
							<li><a href="#tab_productivity" data-toggle="tab">PRODUCTIVITY</a></li>
							<li><a href="#tab_other" data-toggle="tab">OTHER</a></li>
						</ul>
					   <div class="panel-body tab-content">';
		//initializing all content divs
		$tab_content_all = '<div class="tab-pane active list-group list-group-contacts scroll" style="height:305px" id="tab_all_apps">';
		$tab_content_entertainment = '<div class="tab-pane list-group list-group-contacts scroll" style="height:305px" id="tab_entertainment">';
		$tab_content_multimedia = '<div class="tab-pane list-group list-group-contacts scroll" style="height:305px" id="tab_multimedia">';
		$tab_content_connectivity = '<div class="tab-pane list-group list-group-contacts scroll" style="height:305px" id="tab_connectivity">';
		$tab_content_productivity = '<div class="tab-pane list-group list-group-contacts scroll" style="height:305px" id="tab_productivity">';
		$tab_content_other = '<div class="tab-pane list-group list-group-contacts scroll" style="height:305px" id="tab_other">';
		$dom = new domDocument('1.0', 'utf-8'); 
		foreach($response1 as $key => $application_id){
			$found_category = false;
			//variable to store indicator whether to say enable or disable
			$block_unblock_indicator = '';
			// load the html into the object ***/ 
			$dom->loadHTML($application_id['app_category_name']); 
			//discard white space 
			$dom->preserveWhiteSpace = false; 
			$get_app_category = $dom->getElementsByTagName('span'); // here u use your desired tag
			$fetched_category = $get_app_category->item(0)->nodeValue;
			 
			//tab population for communication, social categories
			if ((strpos(strtolower($fetched_category), 'communication') !== false) or (strpos(strtolower($fetched_category), 'social') !== false)) {
				if($application_id['app_is_blocked'] == false){
					$block_unblock_indicator = 'checked="checked"';
				}
				else{
					$block_unblock_indicator = "&nbsp";
				}
				$li = $this->make_application_li($key, $application_id['app_display_name'], $application_id['app_package_name'], $block_unblock_indicator);
				$tab_content_connectivity = $tab_content_connectivity . $li;
				$found_category = true;
			}
			//tab population for entertainment, game categories
			if ((strpos(strtolower($fetched_category), 'entertainment') !== false) or (strpos(strtolower($fetched_category), 'game') !== false)) {
				if($application_id['app_is_blocked'] == false){
					$block_unblock_indicator = 'checked="checked"';
				}
				else{
					$block_unblock_indicator = "&nbsp";
				}
				$li = $this->make_application_li($key, $application_id['app_display_name'], $application_id['app_package_name'], $block_unblock_indicator);
				$tab_content_entertainment = $tab_content_entertainment . $li;
				$found_category = true;
			}
			//tab population for all apps
			if ($application_id['app_package_name'] != "") {
				if($application_id['app_is_blocked'] == false){
					$block_unblock_indicator = 'checked="checked"';
				}
				else{
					$block_unblock_indicator = "&nbsp";
				}
				$li = $this->make_application_li($key, $application_id['app_display_name'], $application_id['app_package_name'], $block_unblock_indicator, "_all");
				$tab_content_all = $tab_content_all . $li;
				//$found_category = true;
			}
			//tab population for music, audio, video, photography categories
			if ((strpos(strtolower($fetched_category), 'music') !== false) or (strpos(strtolower($fetched_category), 'audio') !== false) or (strpos(strtolower($fetched_category), 'video') !== false) or (strpos(strtolower($fetched_category), 'photography') !== false)) {
				if($application_id['app_is_blocked'] == false){
					$block_unblock_indicator = 'checked="checked"';
				}
				else{
					$block_unblock_indicator = "&nbsp";
				}
				$li = $this->make_application_li($key, $application_id['app_display_name'], $application_id['app_package_name'], $block_unblock_indicator);
				$tab_content_multimedia = $tab_content_multimedia . $li;
				$found_category = true;
			}
			//tab population for productivity, books categories
			if ((strpos(strtolower($fetched_category), 'book') !== false) or (strpos(strtolower($fetched_category), 'productivity') !== false)) {
				if($application_id['app_is_blocked'] == false){
					$block_unblock_indicator = 'checked="checked"';
				}
				else{
					$block_unblock_indicator = "&nbsp";
				}
				$li = $this->make_application_li($key, $application_id['app_display_name'], $application_id['app_package_name'], $block_unblock_indicator);
				$tab_content_productivity = $tab_content_productivity . $li;
				$found_category = true;
			}
			//if category not found
			if($found_category == false){
				 if($application_id['app_is_blocked'] == false){
					$block_unblock_indicator = 'checked="checked"';
				}
				else{
					$block_unblock_indicator = "&nbsp";
				}	
				$li = $this->make_application_li($key, $application_id['app_display_name'], $application_id['app_package_name'], $block_unblock_indicator);
				$tab_content_other = $tab_content_other . $li;
			} 
			$found_category = false;
		}
		
		//closing all content divs
		$tab_content_all = $tab_content_all.'</div>';
		$tab_content_entertainment = $tab_content_entertainment.'</div>';
		$tab_content_multimedia = $tab_content_multimedia.'</div>';
		$tab_content_connectivity = $tab_content_connectivity.'</div>';
		$tab_content_productivity = $tab_content_productivity.'</div>';
		$tab_content_other = $tab_content_other.'</div>';
		
		//concatenating all content divs with header
		$tab_header = $tab_header.$tab_content_all.$tab_content_entertainment.$tab_content_multimedia.$tab_content_connectivity.$tab_content_productivity.$tab_content_other;
		//closing header divs
		$tab_header = $tab_header.'</div></div>';
		$final_html = $tab_header;
		//return $response;
		return $final_html;
	}
	
	function make_application_li($key, $app_display_name, $app_package_name, $block_unblock_indicator, $cat_type = ""){
		$li = '<div class="list-group-item">                                    
					<img src="'.base_url().'assets/generic_icons/android_app.png" class="pull-left" alt="'.$app_package_name.'"/>
					<span class="contacts-title">'.$app_display_name.'</span>
					<p>&nbsp
						<label class="switch pull-right" style="margin-right:3%">
							<img src="'.base_url().'assets/img/spinner.gif" id="'.$key.'_load'.$cat_type.'" style="height:25px;width:25px;margin-bottom:12px;display:none">
							<input style="color:white" type="checkbox" class="switch application_dat" cat_type="'.$cat_type.'" id="'.$key.$cat_type.'" data="'.$key.'" '.$block_unblock_indicator.'/>
							<span></span>
						</label>
					</p>                                                                   
				</div>';
		return $li;
	}
}