<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Multimedia_model extends CI_Model{
    
    function __construct()
    {
        parent :: __construct();
    }
	
	function save_multimedia(){
		
		if($this->upload->data()){
			
			//getting file name
			$file_name = $_FILES['file_data']['name'];
			
			//getting media type and user id
			$media_type = $this->input->post('media_type');
			$user_id = $this->input->post('user_id');
			$media_caption = $this->input->post('media_caption');
			$media_description = $this->input->post('media_description');
			
			//setting file title
			$title = pathinfo($file_name, PATHINFO_FILENAME);
					
			//getting extension
			$ext = pathinfo($file_name, PATHINFO_EXTENSION);
				
			//generating file name
			$pic_url_raw = md5(uniqid().$file_name).".".$ext;
			$media_key = md5(uniqid().$file_name);
			
			//generating path to move file to
			$target_file = base_url()."multimedia/".$media_type."/" . $pic_url_raw;
			//base url for multimdeia saved in firebase
			$url = base_url()."multimedia/".$media_type."/";
			$url = $url.$pic_url_raw;
			
			//setting file accessibility parameter
			$is_active = true;
			
			//moving file to server
			$config['upload_path']          = './multimedia/'.$media_type;
			$config['allowed_types']        = 'jpeg|bmp|jpg|png|mp4|3gp|mkv';
			$config['file_name']			= $pic_url_raw;
			$config['max_size']             = 5000;
			//$config['max_width']            = 1024;
			//$config['max_height']           = 768;
			$this->upload->initialize($config);
			$this->load->library('upload', $config);

			if ( !$this->upload->do_upload('file_data')){
				$error = array('error' => $this->upload->display_errors());
				$return['error'] =$error;
				echo json_encode($return);
			}
			else{
				$data = array('upload_data' => $this->upload->data());
				if($data){
					$response['user_id'] = $user_id;
					$response['title'] = $title;
					$response['url'] = $url;
					$response['media_type'] = $media_type;
					$response['is_active'] = $is_active;
					$response['media_key'] = $media_key;
					$response['media_caption'] = $media_caption;
					$response['media_description'] = $media_description;
					$response['success'] ='uploaded';
					return json_encode($response);
				}	
			}
		}
		//on no file or resubmission of file
		else{
			$return['error'] ='File has already been uploaded, please browse again and then upload.';
			echo json_encode($return);
		}
	}
}