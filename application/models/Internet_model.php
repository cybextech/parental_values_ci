<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Internet_model extends CI_Model{
    
    function __construct()
    {
        parent :: __construct();
    }
	
	//method to generate and return tabbed data from internet
	function make_internet_tabs($response){
		
		if($response !== 'null'){
			$decoded_response = json_decode($response, true);
					
			//initializing tabs head
			$tab_header = '<div class="panel panel-default tabs">
							<ul class="nav nav-tabs nav-justified">
								<li class="active"><a href="#tab_sites" data-toggle="tab">WEB SITES</a></li>
								<!--<li><a href="#tab_calls_log" data-toggle="tab">CALLS LOG</a></li>-->
							</ul>
						   <div class="panel-body tab-content">';
			
			//initializing all content divs
			$tab_content_internet = '<div class="tab-pane active list-group list-group-contacts scroll" style="height:325px" id="tab_sites">';
			//$tab_content_calls_log = '<div class="tab-pane list-group list-group-contacts scroll" style="height:325px" id="tab_calls_log">';
			
			foreach($decoded_response as $key_internet => $value_iternet){
				if($value_iternet['is_blocked'] == false){
					$block_unblock_indicator = 'checked="checked"';
				}
				else{
					$block_unblock_indicator = "&nbsp";
				}
				
				$tab_content_internet = $tab_content_internet .'<div class="list-group-item">                                    
																<img src="'.base_url().'assets/generic_icons/internet.png" class="pull-left" alt="'.$value_iternet['site_url'].'"/>
																<span class="contacts-title">'.$value_iternet['site_url'].'</span>
																<p>&nbsp
																	<label class="switch pull-right" style="margin-right:3%">
																		<img src="'.base_url().'assets/img/spinner.gif" id="'.$key_internet.'_load" style="height:25px;width:25px;margin-bottom:12px;display:none"></img>
																		<input style="color:white" type="checkbox" class="switch internet_dat" cat_type="" id="'.$key_internet.'" data="'.$key_internet.'" '.$block_unblock_indicator.'/>
																		<span></span>
																	</label>
																</p>                                                                   
															</div>';
			}
			//closing all content divs
			$tab_content_internet = $tab_content_internet.'</div>';
			//$tab_content_calls_log = $tab_content_calls_log.'</div>';
			
			//concatenating all content divs with header
			$tab_header = $tab_header.$tab_content_internet/* .$tab_content_calls_log */;
			
			//closing header divs
			$tab_header = $tab_header.'</div></div>';
			$final_html = $tab_header;
			
			//return $response;
			return $final_html;
		}
		else{
			return 'no websites found';
		}
			
	}
    
}