<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Home_model extends CI_Model{
    
    function __construct()
    {
        parent :: __construct();
    }
	
	//set device id into session
    function set_device_id_session($device_id = 0, $device_name = ""){
		$this->session->set_userdata('device_id', $device_id);
		$this->session->set_userdata('device_name', $device_name);
		redirect("home/index");
	}
    
}